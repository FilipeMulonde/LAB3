#ifndef COMPSHAPE_H
#define COMPSHAPE_H
#include "shape.hpp"
#include "base-types.hpp"

class CompositeShape : public Shape
{
public:
  CompositeShape();
  CompositeShape(const CompositeShape &comp);
  CompositeShape(CompositeShape &&comp);
  CompositeShape(const Shape &shape);
  ~CompositeShape();

  CompositeShape& operator=(const CompositeShape &other);
  CompositeShape& operator=(CompositeShape &&other);

  void addShape(const Shape &shape);
  void removeShape(size_t index);
  Shape& operator[](size_t index);
  const Shape& operator[](size_t index) const;
  size_t size() const;
  std::unique_ptr<Shape> getCopy() const;

  double getArea() const;
  rectangle_t getFrame() const;
  void move(double dx, double dy);
  void move(const point_t p);
  void scale(double kf);
  void show() const;
  void Show(CompositeShape &shape, int num) const;
  void Show(const Shape &shape) const;

private:
  std::unique_ptr<Shape> *arrayShape_;
  size_t size_, temporary_;
  void startArray(size_t length);
  void restartArray(size_t length);
  void resizeArray();
};

#endif

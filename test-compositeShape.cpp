#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include <math.h>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "CompositeShape.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShape_tests)
BOOST_AUTO_TEST_CASE(compositeShape_matching_shape)
{
  Circle circle(1,4, 5);
  CompositeShape compositeShape(circle);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), circle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrame().height, circle.getFrame().height, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrame().width, circle.getFrame().width, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrame().pos.x, 11, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrame().pos.y, 4, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_1)
{
  Circle circle(61, 18, 7.23);
  Triangle triangle({ -61.5, 2 }, { 71, 0 }, { 14.4, 3.14 });
  Rectangle rectangle(1,5, 10, 12);

  CompositeShape compositeShape(circle);
  compositeShape.addShape(triangle);
  compositeShape.addShape(rectangle);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), circle.getArea() + triangle.getArea() + rectangle.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_2)
{
  Circle circle1(-91, 127.4, 2);
  Triangle triangle1({ -9, 27 }, { 1,-1 }, { 15,81 });
  CompositeShape compositeShape1(circle1);
  compositeShape1.addShape(triangle1);

  Rectangle rectangle1(13,18 , 8, 5);
  Rectangle rectangle2(1, 5, 10, 12);
  Triangle triangle2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  CompositeShape compositeShape2(rectangle1);
  compositeShape2.addShape(rectangle2);
  compositeShape2.addShape(triangle2);

  CompositeShape compositeShape3(compositeShape1);
  compositeShape3.addShape(compositeShape2);
  BOOST_CHECK_CLOSE(compositeShape3.getArea(), compositeShape1.getArea() + compositeShape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape3.getArea(), compositeShape1.getArea() + triangle1.getArea() + rectangle1.getArea()
    + rectangle2.getArea() + triangle2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_3)
{
  Circle circle(-191, 127.4, 22);
  Rectangle rectangle(13,128, 84, 35);
  Triangle triangle({ -11, -12.7 }, { -5, -121 }, { 48.44, 291.666 });
  CompositeShape compositeShape;
  compositeShape.addShape(circle);
  compositeShape.addShape(rectangle);
  compositeShape.addShape(triangle);
  BOOST_CHECK_CLOSE(circle.getArea(), compositeShape[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(rectangle.getArea(), compositeShape[1].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(triangle.getArea(), compositeShape[2].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_4)
{
  Circle circle(-191, 127.4 , 22);
  Rectangle rectangle(13,128, 84, 35);
  CompositeShape compositeShape1;
  compositeShape1.addShape(circle);
  compositeShape1.addShape(rectangle);

  Triangle triangle({ -11, -12.7 }, { -5, -121 }, { 48.44, 291.666 });
  CompositeShape compositeShape2;
  compositeShape2.addShape(compositeShape1);
  compositeShape2.addShape(triangle);

  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(triangle.getArea(), compositeShape2[1].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_dxy)
{
  CompositeShape compositeShape(Triangle({ -9, 27 }, { 1,-1 }, { 15,81 }));
  compositeShape.addShape(Circle(7,19, 6));
  const double width = compositeShape.getFrame().width;
  const double height  = compositeShape.getFrame().height;
  const double area = compositeShape.getArea();
  compositeShape.move(-3.316, 1.257);
  BOOST_CHECK_CLOSE(width, compositeShape.getFrame().width, EPSILON);
  BOOST_CHECK_CLOSE(height , compositeShape.getFrame().height, EPSILON);
  BOOST_CHECK_CLOSE(area , compositeShape.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_point)
{
  CompositeShape compositeShape(Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));
  const double width = compositeShape.getFrame().width;
  const double height  = compositeShape.getFrame().height;
  const double area = compositeShape.getArea();
  compositeShape.move({ 45.8, -3.38 });
  BOOST_CHECK_CLOSE(width, compositeShape.getFrame().width, EPSILON);
  BOOST_CHECK_CLOSE(height , compositeShape.getFrame().height, EPSILON);
  BOOST_CHECK_CLOSE(area , compositeShape.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_assignment)
{
  CompositeShape compositeShape1(Circle(-91, 127.4, 2));
  CompositeShape compositeShape2(Rectangle(13,18, 8, 5));
  compositeShape2 = compositeShape1;
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getFrame().height, compositeShape2.getFrame().height, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getFrame().width, compositeShape2.getFrame().width, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getFrame().pos.x, compositeShape2.getFrame().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getFrame().pos.y, compositeShape2.getFrame().pos.y, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_self_assignment)
{
  CompositeShape compositeShape1(Circle(-91, 127.4, 2));
  CompositeShape compositeShape2 = compositeShape1[0];
  compositeShape2 = compositeShape2;
  BOOST_CHECK(compositeShape2.size() == compositeShape1.size());
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_multiple_assignment)
{
  CompositeShape compositeShape1(Circle(-91, 127.4, 2));
  CompositeShape compositeShape2(Rectangle(-1, 5 , 4, 8));
  CompositeShape compositeShape3(Triangle({ 1,5 }, { 8,1 }, { 2,7 }));

  compositeShape1 = compositeShape2 = compositeShape3;

  BOOST_CHECK(compositeShape1.size() == 1);
  BOOST_CHECK(compositeShape2.size() == 1);
  BOOST_CHECK(compositeShape3.size() == 1);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_complex_assignment)
{
  CompositeShape compositeShape1(Circle(-91, 127.4, 2));
  CompositeShape compositeShape2(Rectangle(-1, 5, 4, 8));
  CompositeShape compositeShape3(compositeShape1);
  compositeShape1 = compositeShape2 = compositeShape3;

  BOOST_CHECK(compositeShape1.size() == 1);
  BOOST_CHECK(compositeShape2.size() == 1);
  BOOST_CHECK(compositeShape3.size() == 1);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_constructor)
{
  Circle circle(1,2, 3);
  CompositeShape compositeShape1(circle);
  CompositeShape compositeShape2(std::move(compositeShape1));
  BOOST_CHECK(compositeShape1.size() == 0);
  BOOST_CHECK_CLOSE(compositeShape2.getArea(), circle.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_operator)
{
  Circle circle(1,2, 3);
  Rectangle rectangle(1,1, 4, 3);
  CompositeShape compositeShape1(circle);
  CompositeShape compositeShape2(rectangle);
  compositeShape2 = std::move(compositeShape1);
  BOOST_CHECK(compositeShape1.size() == 0);
  BOOST_CHECK_CLOSE(compositeShape2.getArea(), circle.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_copy_shape)
{
  Rectangle rectangle(11.89, -54.1, 61.1, 84.8);
  CompositeShape compositeShape(rectangle);
  rectangle.move(-74, 61);
  BOOST_CHECK(std::fabs(rectangle.getFrame().pos.x - compositeShape.getFrame().pos.x) > EPSILON);
  BOOST_CHECK(std::fabs(rectangle.getFrame().pos.y - compositeShape.getFrame().pos.y) > EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale)
{
  Rectangle rectangle1( 0.5, 0.5, 1, 1 );
  Rectangle rectangle2(3, 3, 2, 2 );
  CompositeShape compositeShape1(rectangle1);
  compositeShape1.addShape(rectangle2);

  compositeShape1.scale(0.5);
  BOOST_CHECK_CLOSE(compositeShape1.getFrame().pos.x, 2, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getFrame().pos.y, 2, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getFrame().width, 2, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getFrame().height, 2, EPSILON);

  BOOST_CHECK_CLOSE(compositeShape1[0].getFrame().pos.x, 1.25, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1[0].getFrame().pos.y, 1.25, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1[0].getFrame().width, 0.5, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1[0].getFrame().height, 0.5, EPSILON);

  BOOST_CHECK_CLOSE(compositeShape1[1].getFrame().pos.x, 2.5, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1[1].getFrame().pos.y, 2.5, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1[1].getFrame().width, 1, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1[1].getFrame().height, 1, EPSILON);

}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_1)
{
  CompositeShape compositeShape(Circle(13,18, 8));
  compositeShape.addShape(Triangle({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 }));
  compositeShape.addShape(Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));
  const double area = compositeShape.getArea();
  const double kf = 0.77;
  compositeShape.scale(kf);
  BOOST_CHECK_CLOSE(area * kf * kf, compositeShape.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_2)
{
  Circle circle(13,18, 8);
  Triangle triangle({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });

  CompositeShape compositeShape(circle);
  compositeShape.addShape(triangle);

  const double kf = 1.337;
  compositeShape.scale(kf);
  circle.scale(kf);
  triangle.scale(kf);
  BOOST_CHECK_CLOSE(circle.getArea() + triangle.getArea(), compositeShape.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_3)
{
  CompositeShape compositeShape1(Circle(-91, 127.4, 2));
  compositeShape1.addShape(Triangle({ -9, 27 }, { 1,-1 }, { 15,81 }));

  CompositeShape compositeShape2(Rectangle(13,18, 8, 5));
  compositeShape2.addShape(Rectangle(1, 5 , 10, 12));
  compositeShape2.addShape(Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));

  CompositeShape compositeShape3(compositeShape1);
  compositeShape3.addShape(compositeShape2);

  const double kf = 2.017;
  compositeShape1.scale(kf);
  compositeShape2.scale(kf);
  compositeShape3.scale(kf);

  CompositeShape compositeShape4(compositeShape1);
  compositeShape4.addShape(compositeShape2);

  BOOST_CHECK_CLOSE(compositeShape3.getArea(), compositeShape1.getArea() + compositeShape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape4.getArea(), compositeShape3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_center)
{
  CompositeShape compositeShape(Circle(13,18, 8));
  compositeShape.addShape(Triangle({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 }));
  compositeShape.addShape(Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));
  const point_t center = compositeShape.getFrame().pos;
  const double kf = 1.37;
  compositeShape.scale(kf);
  BOOST_CHECK_CLOSE(center.x, compositeShape.getFrame().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(center.y, compositeShape.getFrame().pos.y, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_remove_test_1)
{
  Triangle triangle1({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });
  Triangle triangle2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  CompositeShape compositeShape1;
  compositeShape1.addShape(triangle1);
  compositeShape1.addShape(triangle2);

  BOOST_CHECK_CLOSE(compositeShape1[0].getArea(), triangle1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1[1].getArea(), triangle2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), triangle1.getArea() + triangle2.getArea(), EPSILON);

  compositeShape1.removeShape(1);

  BOOST_CHECK(compositeShape1.size() == 1);
  BOOST_CHECK_CLOSE(compositeShape1[0].getArea(), triangle1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), triangle1.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_remove_test_2)
{
  Triangle triangle1({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });
  Triangle triangle2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  CompositeShape compositeShape1;
  compositeShape1.addShape(triangle1);
  compositeShape1.addShape(triangle2);

  BOOST_CHECK_CLOSE(compositeShape1[0].getArea(), triangle1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1[1].getArea(), triangle2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), triangle1.getArea() + triangle2.getArea(), EPSILON);

  compositeShape1.removeShape(0);

  BOOST_CHECK(compositeShape1.size() == 1);
  BOOST_CHECK_CLOSE(compositeShape1[0].getArea(), triangle2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), triangle2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_invalid_scale_1)
{
  CompositeShape compositeShape(Circle(1, 2, 3));
  BOOST_CHECK_THROW(compositeShape.scale(0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(compositeShape_invalid_scale_2)
{
  CompositeShape compositeShape(Circle(1, 2, 3));
  BOOST_CHECK_THROW(compositeShape.scale(-2), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(compositeShape_operator_out_of_range)
{
  CompositeShape compositeShape(Circle(1,2, 3));
  BOOST_CHECK_THROW(compositeShape[5], std::out_of_range);
}
BOOST_AUTO_TEST_CASE(compositeShape_removeShape_out_of_range)
{
  CompositeShape compositeShape(Circle(1,2, 3));
  BOOST_CHECK_THROW(compositeShape.removeShape(3), std::out_of_range);
}
BOOST_AUTO_TEST_CASE(compositeShape_empty_getArea)
{
  CompositeShape compositeShape;
  BOOST_CHECK_CLOSE(compositeShape.getArea(), 0, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_empty_getFrameRect)
{
  CompositeShape compositeShape;
  BOOST_CHECK_CLOSE(compositeShape.getFrame().height, 0, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrame().width, 0, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrame().pos.x, 0, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrame().pos.y, 0, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_copy_constructor)
{
  CompositeShape compositeShape1(Circle(1,2, 3));
  compositeShape1.addShape(Rectangle( 4,5 , 6, 7));

  CompositeShape compositeShape2(compositeShape1);

  BOOST_CHECK(compositeShape2.size() == compositeShape1.size());
  BOOST_CHECK_CLOSE(compositeShape2[0].getArea(), compositeShape1[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape2[1].getArea(), compositeShape1[1].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_const_copy_1)
{
  Circle circle( 4,11 , 15);
  const double initialArea = circle.getArea();

  CompositeShape compositeShape(circle);
  compositeShape.scale(3);

  BOOST_CHECK_CLOSE(circle.getArea(), initialArea, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_const_copy_2)
{
  Circle circle( 4,11 , 15);
  const double initialArea = circle.getArea();

  CompositeShape compositeShape(circle);
  compositeShape[0].scale(3);

  BOOST_CHECK_CLOSE(circle.getArea(), initialArea, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

#include "CompositeShape.hpp"
#include <stdexcept>
#include <algorithm>
#include <iostream>

CompositeShape::CompositeShape()
{
  startArray(1);
}
CompositeShape::CompositeShape(const CompositeShape &comp)
{
  startArray(comp.temporary_);
  *this = comp;
}
CompositeShape::CompositeShape(const Shape &shape)
{
  startArray(1);
  addShape(shape);
}
CompositeShape::~CompositeShape()
{
  delete[] arrayShape_;
  arrayShape_ = nullptr;
}
CompositeShape::CompositeShape(CompositeShape &&comp)
{
  size_ = comp.size_;
  comp.size_ = 0;

  temporary_ = comp.temporary_;
  comp.temporary_ = 0;

  arrayShape_ = comp.arrayShape_;
  comp.arrayShape_ = nullptr;
}

CompositeShape& CompositeShape::operator=(const CompositeShape &other)
{
  if(this != &other)
  {
    if(temporary_ >= other.temporary_)
    {
      for(size_t i = 0; i < size_; i++)
      {
        arrayShape_[i].reset(nullptr);
      }
      size_ = 0;
    }
    else
    {
      restartArray(other.temporary_);
    }
    for(size_t i = 0; i < other.size(); i++)
    {
      addShape(other[i]);
    }
  }
  return *this;
}
CompositeShape& CompositeShape::operator=(CompositeShape &&other)
{
  if(this != &other)
  {
    delete[] arrayShape_;
    arrayShape_ = other.arrayShape_;
    other.arrayShape_ = nullptr;

    size_ = other.size_;
    other.size_ = 0;

    temporary_ = other.temporary_;
    other.temporary_ = 0;
  }
  return *this;
}
void CompositeShape::startArray(size_t length)
{
  arrayShape_ = new std::unique_ptr<Shape>[length];
  temporary_ = length;
  size_ = 0;
}
void CompositeShape::resizeArray()
{
  std::unique_ptr<Shape> *temp = new std::unique_ptr<Shape>[2 * temporary_];
  for(size_t i = 0; i < size_; i++)
  {
    temp[i] = std::move(arrayShape_[i]);
  }
  delete[] arrayShape_;
  arrayShape_ = temp;
  temporary_ *= 2;
}
void CompositeShape::restartArray(size_t length)
{
  std::unique_ptr<Shape> *temp = new std::unique_ptr<Shape>[length];
  delete[] arrayShape_;
  arrayShape_ = temp;
  temporary_ = length;
  size_ = 0;
}
Shape& CompositeShape::operator[](size_t index)
{
  if(index >= size_)
  {
    throw std::out_of_range("CompositeShape index out of range");
  }
  return *arrayShape_[index];
}
const Shape& CompositeShape::operator[](size_t index) const
{
  if(index >= size_)
  {
    throw std::out_of_range("CompositeShape index out of range");
  }
  return *arrayShape_[index];
}
void CompositeShape::removeShape(size_t index)
{
  if(index >= size_)
  {
    throw std::out_of_range("CompositeShape index out of range");
  }
  arrayShape_[index].reset(nullptr);
  for(size_t i = index + 1; i < size_; i++)
  {
    arrayShape_[i - 1] = std::move(arrayShape_[i]);
  }
  arrayShape_[--size_].reset(nullptr);
}
size_t CompositeShape::size() const
{
  return size_;
}
void CompositeShape::addShape(const Shape &shape)
{
  if(size_ == temporary_)
  {
    resizeArray();
  }
  arrayShape_[size_++] = std::move(shape.getCopy());
}
std::unique_ptr<Shape> CompositeShape::getCopy() const
{
  return std::unique_ptr<CompositeShape>(new CompositeShape(*this));
}


double CompositeShape::getArea() const
{
  double area = 0;
  for(size_t i = 0; i < size_; i++)
  {
    area += arrayShape_[i]->getArea();
  }
  return area;
}
rectangle_t CompositeShape::getFrame() const
{
  if(size_ == 0)
  {
    return { { 0, 0 }, 0, 0 };
  }

  rectangle_t r0 = arrayShape_[0]->getFrame();
  double left = r0.pos.x - r0.width / 2;
  double right = r0.pos.x + r0.width / 2;
  double bot = r0.pos.y - r0.height / 2;
  double top = r0.pos.y + r0.height / 2;

  using namespace std;
  for(size_t i = 1; i < size_; i++)
  {
    rectangle_t ri = arrayShape_[i]->getFrame();
    left = min(left, ri.pos.x - ri.width / 2);
    right = max(right, ri.pos.x + ri.width / 2);
    bot = min(bot, ri.pos.y - ri.height / 2);
    top = max(top, ri.pos.y + ri.height / 2);
  }
  return { (left + right) / 2, (bot + top) / 2, right - left, top - bot };
}
void CompositeShape::move(double dx, double dy)
{
  for(size_t i = 0; i < size_; i++)
  {
    arrayShape_[i]->move(dx, dy);
  }
}
void CompositeShape::move(const point_t p)
{
  point_t center = getFrame().pos;
  move(p.x - center.x, p.y - center.y);
}
void CompositeShape::scale(double kf)
{
  if(kf <= 0)
  {
    throw std::invalid_argument("Invalid CompositeShape scale coefficient!");
  }
  point_t compCenter = getFrame().pos;
  for(size_t i = 0; i < size_; i++)
  {
    arrayShape_[i]->scale(kf);
    point_t shapeCenter = arrayShape_[i]->getFrame().pos;
    arrayShape_[i]->move((compCenter.x - shapeCenter.x) * kf, (compCenter.y - shapeCenter.y) * kf);
  }
}
void CompositeShape::show() const{

}

void CompositeShape::Show(const Shape &shape) const
{
  std::cout << "Area: " << shape.getArea() << std::endl;
  rectangle_t frame = shape.getFrame();
  std::cout << "Frame Rectangle - X: " << frame.pos.x << "; Y: " << frame.pos.y << std::endl;
  std::cout << "Frame Rectangle - Width: " << frame.width << "; Height: " << frame.height << "\n\n";
}
void CompositeShape::Show(CompositeShape &shape, int num) const
{
  Show(shape);
  std::cout << "ComplexShape" << num << " elements:" << std::endl;
  std::cout << "Size = " << shape.size() << std::endl;
  for(size_t i = 0; i < shape.size(); i++)
  {
    Show(shape[i]);
  }
}

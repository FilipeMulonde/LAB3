#ifndef CIRCLE_H
#define CIRCLE_H
#include "shape.hpp"
#include "base-types.hpp"
#include <memory>

	class Circle :public Shape {
		public:
			Circle(double centerX, double centerY, double radius);
			double getArea() const;
			rectangle_t getFrame() const;
			void move(double dx, double dy);
			void move(point_t p);
			std::unique_ptr<Shape> getCopy() const;
			void show() const;
			void scale(const double kf);
		private:
			point_t center_;
			double radius_;
	};

#endif

#include <iostream>
#include "CompositeShape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "base-types.hpp"

int main()
{
	std::cout << "rectangle x:11, y:13, W:5, h:5" << std::endl;
	Rectangle rectangle(11, 13, 5, 5);
	rectangle.show();

	std::cout << "moving the rectangle DX:5, dy:10" << std::endl;
	rectangle.move(5, 10);
	rectangle.show();

	std::cout << "Move the rectangle  x:20.41, y:13.11" << std::endl;
	rectangle.move({ 20.41, 13.11 });
	rectangle.show();

	std::cout << "Scaling rectangle  kf:2.9" << std::endl;
	rectangle.scale(2.9);
	rectangle.show();

  std::cout << "Creating complexShape1 and Adding Rectangle" << std::endl;
  CompositeShape CompositeShape1(rectangle);
  CompositeShape1.Show(CompositeShape1, 1);

	std::cout << "circle X in a:15 d:19 p:4" << std::endl;
 	Circle circle (15, 19, 4);
 	circle.show();

 	std::cout << "Move the circle x:12.41, y:20" << std::endl;
 	circle.move({ 12.41, 20 });
 	circle.show();

 	std::cout << "Move the circle dx:4.4, dy:20.2" << std::endl;
 	circle.move(4.4, 20.2);
 	circle.show();

 	std::cout << "Scaling circle  k:0.82" << std::endl;
 	circle.scale(0.82);
 	circle.show();

  std::cout << "Adding Circle to complexShape1" << std::endl;
	CompositeShape1.addShape(circle);
	CompositeShape1.Show(CompositeShape1, 2);

	std::cout << "Triangle point a(11,30), B(55,6), C(45,36)" << std::endl;
	Triangle triangle({ 11,30 }, { 55,6 }, { 45,36 });
	triangle.show();

	std::cout << "Move triangle Dx : 1.8, dy : 5" << std::endl;
	triangle.move(1.8, 5);
	triangle.show();

	std::cout << "Move triangle dx : 23, dy : 6" << std::endl;
	triangle.move({23, 6});
	triangle.show();

	std::cout << "Scaling triangle by k:1.5" << std::endl;
	triangle.scale({1.5});
	triangle.show();

	std::cout << "Adding Triangle to complexShape1" << std::endl;
	CompositeShape1.addShape(triangle);
	CompositeShape1.Show(CompositeShape1, 2);

  std::cout << "Moving CompositeShape1 by dx:13, dy:9" << std::endl;
  CompositeShape1.move(13, 9);
  CompositeShape1.Show(CompositeShape1, 1);

  std::cout << "Moving CompositeShape1 to point x:11, y:8" << std::endl;
  CompositeShape1.move({ 11,8 });
  CompositeShape1.Show(CompositeShape1, 1);

	std::cout << "Scaling CompositeShape1 by k:1.4" << std::endl;
  CompositeShape1.scale(1.4);

  std::cout << "Crectangleeating CompositeShape2 frectangleom CompositeShape1" << std::endl;
  CompositeShape CompositeShape2(CompositeShape1);
  CompositeShape2.Show(CompositeShape2, 2);

  std::cout << "Scaling CompositeShape2 by k=1.4" << std::endl;
  CompositeShape2.scale(1.4);
  CompositeShape2.Show(CompositeShape2, 2);

  std::cout << "Adding Trectangleiangle to CompositeShape2" << std::endl;
  CompositeShape2.addShape(triangle);
  CompositeShape2.Show(CompositeShape2, 2);

  std::cout << "Assigning CompositeShape2 to CompositeShape1" << std::endl;
  CompositeShape1 = CompositeShape2;
  CompositeShape1.Show(CompositeShape1, 1);

  std::cout << "Scaling CompositeShape2 by k=1.4" << std::endl;
  CompositeShape2.scale(1.4);
  CompositeShape1.Show(CompositeShape2, 2);

  return 0;
}

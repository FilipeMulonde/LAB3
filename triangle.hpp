#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "shape.hpp"
#include "base-types.hpp"
#include <iostream>

	class Triangle :public Shape {
	public:
		Triangle(point_t p1, point_t p2, point_t p3);
		double getArea() const;
		rectangle_t getFrame() const;
		void move(double dx, double dy);
		void move(point_t p);
		std::unique_ptr<Shape> getCopy() const override;
		void scale(const double kf);
		void show() const;

	private:
		point_t a_, b_, c_;
		point_t findCenter() const;
		double  findDistance(point_t p1, point_t p2) const;
	};

#endif

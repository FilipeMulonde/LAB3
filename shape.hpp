#ifndef SHAPE_H
#define SHAPE_H
#include "base-types.hpp"
#include <memory>
	class Shape
	{
		public:
			virtual ~Shape() = default;
			virtual double getArea() const = 0;
			virtual rectangle_t getFrame() const = 0;
			virtual void move(double dx, double dy) = 0;
			virtual void move(point_t p) = 0;
			virtual void scale(const double kf) = 0;
		  virtual void show() const = 0;
			virtual std::unique_ptr<Shape> getCopy() const = 0;

	};

#endif

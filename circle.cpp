#include "circle.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <stdexcept>

Circle::Circle(double centerX, double centerY, double radius):
	center_({centerX, centerY }),
	radius_(radius)
{
	if(radius_ <= 0)
		{
		  throw std::invalid_argument("Invalid circle parameters try the values should be Greater than 0!");
	  }
}
double Circle::getArea() const
{
	return radius_*radius_*M_PI;
}
rectangle_t Circle::getFrame() const
{
	return rectangle_t{ center_, radius_ * 2, radius_ * 2 };
}
void Circle::move(double dx, double dy)
{
	center_.x += dx;
	center_.y += dy;
}

std::unique_ptr<Shape> Circle::getCopy() const
{
  return std::unique_ptr<Shape>(new Circle(center_.x, center_.y, radius_));
}

void Circle::move(point_t p)
{
	center_ = p;
}
void Circle::scale(const double kf)
{
  if(kf <= 0)
  {
    throw std::invalid_argument("Invalid Circle scale coefficient, should be Greater then 0");
  }
	else
	{
		radius_ *= kf;
	}
}
void Circle::show() const
{
	std::cout << "Area of Circle: " << getArea() << std::endl;
	rectangle_t frame = getFrame();
	std::cout << "Frame X : " << frame.pos.x << "Frame Y: " << frame.pos.y << std::endl;
	std::cout << "Frame -Width: " << frame.width << "Frame Height: " << frame.height << "\n\n";
}
